import Vue from 'vue'
import Router from 'vue-router'

const Container = () => import('@/views/containers/layout')
const Jawaban1 = () => import('@/views/jawaban1/index')
const Default = () => import('@/views/default/index')
const Login = () => import('@/views/login/index')
const Register = () => import('@/views/login/reset')
const Starships = () => import('@/views/pages/starships/index')
const Adduser = () => import('@/views/pages/starships/add')
const Updateuser = () => import('@/views/pages/starships/update')

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      component: () => import('@/views/containers/full-pages.vue'),
      children: [
        {
          path: '/',
          name: 'Default',
          component: Default,
          meta: {
            guest: true
          }
        },
        {
          path: '/Login',
          name: 'Login',
          component: Login,
          meta: {
            guest: true
          }
        },
        {
          path: '/Jawaban1',
          name: 'Jawaban1',
          component: Jawaban1,
          meta: {
            guest: true
          }
        },
        {
          path: '/Registrasi',
          name: 'Register',
          component: Register,
          meta: {
            guest: true
          }
        }
      ]
    },
    {
      path: '/',
      redirect: '/starships',
      name: 'Starships',
      component: Container,
      children: [
        {
          path: 'starships',
          name: 'Starships',
          component: Starships
        },
        {
          path: '/add-user',
          name: 'Adduser',
          component: Adduser,
          meta: {
            guest: true
          }
        },
        {
          path: '/update-user/:id',
          name: 'Updateuser',
          component: Updateuser,
          meta: {
            guest: true
          }
        }
      ]
    }
  ]
})
