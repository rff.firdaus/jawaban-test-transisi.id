import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import infiniteScroll from 'vue-infinite-scroll'
import Notifications from 'vue-notification'
Vue.use(Notifications)
require('bootstrap-vue-datatable')
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(infiniteScroll)
Vue.config.productionTip = false

axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  let res = error.response
  let messages = ''
  if (res.status === 400) {
    messages = 'Missing password'
  } else if (res.status === 404) {
    messages = 'Data Not Found'
  } else if (res.status === 500) {
    messages = 'Sedang Terjadi Gangguan'
  }
  Vue.notify({
    group: 'foo',
    title: 'Peringatan',
    text: messages,
    type: 'error',
    max: 3
  })
  return Promise.reject(error)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
